package businessLogicLayer;

import dataAccessLayer.ClientDAO;
import dataAccessLayer.OrderDAO;
import dataAccessLayer.OrderDetailsDAO;
import dataAccessLayer.ProductDAO;
import model.Client;
import model.Order;
import model.OrderDetail;
import model.Product;
import presentation.PdfFileGenerator;
import presentation.TextFileParser;

import java.util.ArrayList;

/**
 * It encapsulates the application logic.
 */
public class AppManager {

    /**
     * Inserts a new client into the database.
     * <p>
     *     If there is no existing client with the same name and address, it inserts the new client.
     * </p>
     * @param tokens The first element of the array represents the name of the command, the second one
     *               represents the name of the client and the third one the client's address
     */
    public static void insertClientIntoDatabase(String[] tokens) {

        Client newClient = new Client(1, tokens[1], tokens[2]);
        ArrayList<Client> clients = ClientDAO.findClientByName(newClient.getName());

        if (clients.isEmpty())
            ClientDAO.insertClient(newClient);
        else {

            boolean clientFoundInDatabase = false;

            for(Client client: clients)
                if (client.getName().equals(newClient.getName()) == true && client.getAddress().equals(newClient.getAddress()) == true) {
                    clientFoundInDatabase = true;
                    break;
                }

            if (clientFoundInDatabase == false)
                ClientDAO.insertClient(newClient);
            else
                System.out.println("Client already exists in the database!");
        }
    }


    /**
     * Removes a client from database.
     * <p>
     *     If there exists a client in the database with the specified name and address, it will be removed from database.
     * </p>
     * @param tokens The first element of the array represents the name of the command, the second one
     *               represents the name of the client and the third one the client's address
     */
    public static void deleteClientFromDatabase(String[] tokens) {

        Client newClient = new Client(1, tokens[1], tokens[2]);
        Client foundClient = ClientDAO.findClientByNameAndAddress(newClient.getName(), newClient.getAddress());

        if (foundClient != null) {

            ArrayList<Order> ordersMadeByFoundClient = OrderDAO.findOrderByClientId(foundClient.getId());
            for (int index = 0; index < ordersMadeByFoundClient.size(); index++) {
                OrderDetailsDAO.deleteOrderDetails(ordersMadeByFoundClient.get(index).getId());
                OrderDAO.deleteOrder(ordersMadeByFoundClient.get(index).getId()); //order.getId()
            }

            ClientDAO.deleteClient(foundClient.getId());
        }
        else {
            System.out.println("The client doeesn't exist in the application database!");
        }

    }


    /**
     * Inserts a new product into the database.
     * <p>
     *     If there is no existing product with the same name, it will be inserted into the database, otherwise
     *     the stock/quantity and its price will be updated to the new specified values.
     * </p>
     * @param tokens The first element of the array represents the name of the command, the second one
     *               represents the name of the client and the third one the client's address
     */
    public static void insertProductIntoDatabase(String[] tokens) {

        Product newProduct = new Product(1, tokens[1], Integer.parseInt(tokens[2]), Double.parseDouble(tokens[3]));
        Product foundProduct = ProductDAO.findProductByName(newProduct.getName());

        if (foundProduct == null) {
            ProductDAO.insertProduct(newProduct);
        }
        else {
            ProductDAO.updateProduct(foundProduct.getId(), newProduct.getName(), newProduct.getQuantity(), newProduct.getPrice());
        }
    }


    /**
     * Removes a product from database.
     * <p>
     *     If there is a product with the specified name, it will be removed from database.
     * </p>
     * @param tokens The first element of the array represents the name of the command, the second one
     *               represents the name of the client and the third one the client's address
     */
    public static void deleteProductFromDatabase(String[] tokens) {

        Product newProduct = new Product(1, tokens[1], 1, 1);
        Product foundProduct = ProductDAO.findProductByName(newProduct.getName());

        if (foundProduct != null) {
            ProductDAO.deleteProduct(foundProduct.getId());
            System.out.println("Delete product succesfully completed!");
        }
        else {
            System.out.println("The product doeesn't exist in the application database!");
        }
    }


    /**
     * It creates a new order into database.
     * <p>
     *     If there is a client with the specified name, a product with the specified name and also the product
     *     has a greater quantity than it is requested, the new order will be inserted into the database along with
     *     its order details. The order id and order details id are equal/equivalent/the same. If the order was
     *     succesfully inserted, a pdf report representing a bill will be generated, otherwise a pdf report representing
     *     an under-stock message will be generated.
     * </p>
     * @param tokens The first element of the array represents the name of the command, the second one
     *               represents the name of the client and the third one the client's address
     */
    public static void createOrder(String[] tokens) {
        /**  Order: Sandu Vasile, apple, 100 */

        Client foundClientInDatabase = ClientDAO.findClientByName(tokens[1]).get(0);
        Product foundProductInDatabase = ProductDAO.findProductByName(tokens[2]);
        int requestedProductQuantity = Integer.parseInt(tokens[3]);
        double totalPrice = foundProductInDatabase.getPrice() * requestedProductQuantity;


        if (foundClientInDatabase != null &&  foundProductInDatabase != null && foundProductInDatabase.getQuantity() >= requestedProductQuantity) {
            Order order = new Order(1, foundClientInDatabase.getId(), foundProductInDatabase.getId(), requestedProductQuantity, 1);
            OrderDetail orderDetail = new OrderDetail(1, totalPrice);

            OrderDAO.createOrder(order);
            OrderDetailsDAO.createOrderDetails(orderDetail);
            OrderDAO.updateOrder(order.getId(), orderDetail.getId());
            ProductDAO.updateProduct(foundProductInDatabase.getId(), foundProductInDatabase.getName(), foundProductInDatabase.getQuantity() - requestedProductQuantity, foundProductInDatabase.getPrice());
            PdfFileGenerator.PdfBill(order, foundProductInDatabase, foundClientInDatabase, orderDetail, requestedProductQuantity);
        }
        else if (foundProductInDatabase.getQuantity() < requestedProductQuantity) {
            PdfFileGenerator.PdfReportUnderStockMessage(foundProductInDatabase);
        }
        else {
            System.out.println("No available client / Product not available");
        }
    }

    /**
     * It reads the input commands from the txt file.
     * @param path The path for the input/commands txt file
     */
    public static void startApp(String path) {
        TextFileParser.parseCommandsTextFile(path);
    }


    /**
     * The main function of the application which starts the entire process of the application.
     * @param args the path where it is the command text file.
     */
    public static void main(String[] args) {
        startApp(args[0]);
    }
}
