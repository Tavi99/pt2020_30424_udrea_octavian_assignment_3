package connection;

import java.sql.*;

/**
 * It contains info about the database and makes the connections with it.
 */
public class ConnectionFactory {

    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/ordermanagement";
    private static final String USER = "root";
    private static final String PASSWORD = "g4lCrT32w9p";

    /**
     * It creates the connection with the database.
     * @return An object of type Connection
     * @throws Exception exception if there occurs when trying to connect to the database
     */
    public static Connection getConnection() throws Exception {
        try {
            Class.forName(DRIVER);
            Connection connection = DriverManager.getConnection(DBURL, USER, PASSWORD);

            return connection;
        } catch (Exception e) {
            System.out.println(e);
        }

        return null;
    }

    /**
     * It closes the connection with database.
     * @param connection the connection with database
     */
    public static void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                System.out.println("Cannot close the connection!");
            }
        }
    }

    /**
     * It closes the statement connection.
     * @param statement statement connection
     */
    public static void close(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                System.out.println("Cannot close the statement!");
            }
        }
    }

    /**
     * It closes the result set.
     * @param resultSet the result set from database
     */
    public static void close(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                System.out.println("Cannot close the result set!");
            }
        }
    }

}
