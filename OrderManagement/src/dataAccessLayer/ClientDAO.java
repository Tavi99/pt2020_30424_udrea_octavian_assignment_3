package dataAccessLayer;

import connection.ConnectionFactory;
import model.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Contains the queries and database connections for managing clients.
 */
public class ClientDAO {

    private final static String insertStatementString = "INSERT INTO client (name, address) VALUES (?, ?)";
    private final static String deleteStatementString = "DELETE from client where id = ?";
    private final static String findByNameStatementString = "SELECT * FROM client where name = ?";
    private final static String findByNameAndAddressStatementString = "SELECT * FROM client where name = ? and address = ?";
    private final static String selectStatementString = "SELECT * FROM client";

    /**
     * It inserts a new client into database.
     * @param client the client which will be inserted into database.
     */
    public static void insertClient(Client client) {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(insertStatementString);
            statement.setString(1, client.getName());
            statement.setString(2, client.getAddress());
            statement.executeUpdate();
            System.out.println("Insertion complete");

            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * It removes a client from database.
     * @param clientId the id of the client which will be removed from database.
     */
    public static void deleteClient(int clientId) {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(deleteStatementString);
            statement.setInt(1, clientId);
            statement.executeUpdate();
            System.out.println("Deletion complete");

            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * It finds and returns all the clients with the specified name, and also all the orders which were
     * made by him.
     * @param name the name of the client which is searched for in the database.
     * @return an array list with objects of type Client, which represents all the clients which have the specified name.
     */
    public static ArrayList<Client> findClientByName(String name) {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(findByNameStatementString);
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();

            ArrayList<Client> clientsArray = new ArrayList<>();

            while(resultSet.next()) {
                Client client = new Client(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("address"));
                clientsArray.add(client);
            }

            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(resultSet);

            return clientsArray;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    /**
     * It finds and returns the client with the specified name and address if it exists one.
     * @param name the name of the client which is searched for in the database.
     * @param address the address of the client which is searched for in the database.
     * @return an object of type Client, which has the specified name and address, otherwise null.
     */
    public static Client findClientByNameAndAddress(String name, String address) {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(findByNameAndAddressStatementString);
            statement.setString(1, name);
            statement.setString(2, address);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                Client client = new Client(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("address"));
                return client;
            }

            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(resultSet);

            return null;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    /**
     * It returns all the clients existing in database.
     * @return an array list with objects of type Client, or null if there are no clients in the database.
     */
    public static ArrayList<Client> showAllClients() {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(selectStatementString);
            ResultSet resultSet = statement.executeQuery();

            ArrayList<Client> clientsArray = new ArrayList<>();

            while(resultSet.next()) {
                Client client = new Client(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("address"));
                clientsArray.add(client);
            }

            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(resultSet);

            return clientsArray;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

}













