package dataAccessLayer;

import connection.ConnectionFactory;
import model.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Contains the queries and database connections for managing products.
 */
public class ProductDAO {

    private final static String insertStatementString = "INSERT INTO product (name, quantity, price) VALUES (?, ?, ?)";
    private final static String deleteStatementString = "DELETE from product where id = ?";
    private final static String updateStatementString = "UPDATE product SET name = ?, quantity = ?, price = ? where id = ?";
    private final static String findByNameStatementString = "SELECT * FROM product where name = ?";
    private final static String selectStatementString = "SELECT * FROM product";

    /**
     * Inserts a product into database.
     * @param product the product which will be inserted into database.
     */
    public static void insertProduct(Product product) {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(insertStatementString);
            statement.setString(1, product.getName());
            statement.setInt(2, product.getQuantity());
            statement.setDouble(3, product.getPrice());
            statement.executeUpdate();

            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Removes a product from database.
     * @param productId the id of the product which will be removed from database.
     */
    public static void deleteProduct(int productId) {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(deleteStatementString);
            statement.setInt(1, productId);
            statement.executeUpdate();

            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * It updates the name, quantity and price of an existing product in the database.
     * @param productId the id of the product
     * @param newName the new name of the product
     * @param newQuantity the new quantity for that product
     * @param newPrice the new price for that product
     */
    public static void updateProduct(int productId, String newName, int newQuantity, double newPrice) {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(updateStatementString);
            statement.setString(1, newName);
            statement.setInt(2, newQuantity);
            statement.setDouble(3, newPrice);
            statement.setInt(4, productId);
            statement.executeUpdate();

            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * It returns a product with the specified name.
     * @param name the name of the product which is searched for.
     * @return an object of type Product if there exists any, otherwise null.
     */
    public static Product findProductByName(String name) {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(findByNameStatementString);
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Product product = new Product(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getInt("quantity"), resultSet.getDouble("price"));
                return product;
            }
            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(resultSet);
            return null;
        } catch (Exception e) { System.out.println(e); }
        return null;
    }

    /**
     * It returns all the products from the database.
     * @return an array list with objects of type Product, otherwise null(if there are no products in database).
     */
    public static ArrayList<Product> showAllProducts() {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(selectStatementString);
            ResultSet resultSet = statement.executeQuery();
            ArrayList<Product> productArrayList = new ArrayList<>();
            while(resultSet.next()) {
                Product product = new Product(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getInt("quantity"), resultSet.getDouble("price"));
                productArrayList.add(product);
            }
            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(resultSet);
            return productArrayList;
        } catch (Exception e) { System.out.println(e); }
        return null;
    }
}
