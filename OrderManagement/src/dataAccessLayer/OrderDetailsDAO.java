package dataAccessLayer;

import connection.ConnectionFactory;
import model.OrderDetail;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * Contains the queries and database connections for managing order details.
 */
public class OrderDetailsDAO {

    private final static String insertStatementString = "INSERT INTO orderdetails (totalPrice) VALUES (?)";
    private final static String deleteStatementString = "DELETE from orderdetails where id = ?";

    /**
     * It inserts into database the details of an order(the total price of the order) which is associated with it.
     * @param orderDetail the details of an order which will be inserted into database.
     */
    public static void createOrderDetails(OrderDetail orderDetail) {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(insertStatementString);
            statement.setDouble(1, orderDetail.getTotalPrice());
            statement.executeUpdate();

            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * It removes from database the details of an order which is associated with it.
     * @param orderId the id of the details order which will be removed from database.
     */
    public static void deleteOrderDetails(int orderId) {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(deleteStatementString);
            statement.setInt(1, orderId);
            statement.executeUpdate();
            System.out.println("Deletion complete");

            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
