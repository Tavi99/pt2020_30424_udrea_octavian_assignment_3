package dataAccessLayer;

import connection.ConnectionFactory;
import model.Order;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Contains the queries and database connections for managing orders.
 */
public class OrderDAO {

    private final static String insertStatementString = "INSERT INTO ordermanagement.order (clientId, productId, quantity, orderDetailsId) VALUES (?, ?, ?, ?)";
    private final static String updateStatementString = "UPDATE ordermanagement.order SET orderDetailsId = ? where id = ?";
    private final static String findByClientIdStatementString = "SELECT * FROM ordermanagement.order where clientId = ?";
    private final static String deleteStatementString = "DELETE from ordermanagement.order where id = ?";
    private final static String selectStatementString = "SELECT * FROM ordermanagement.order";

    /**
     * It inserts into database a new order.
     * @param order the order which will be inserted into the database.
     */
    public static void createOrder(Order order) {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(insertStatementString);
            statement.setInt(1, order.getClientId());
            statement.setInt(2, order.getProductId());
            statement.setInt(3, order.getQuantity());
            statement.setInt(4, order.getOrderDetailsId());
            statement.executeUpdate();

            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * It updates the order details and the ids, so that the order id and order details id are the same.
     * @param orderId the id of the order.
     * @param orderDetailId the id of the details order.
     */
    public static void updateOrder(int orderId, int orderDetailId) {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(updateStatementString);
            statement.setInt(1, orderDetailId);
            statement.setInt(2, orderId);
            statement.executeUpdate();

            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * It removes an order from database.
     * @param orderId the id of the order which will be removed from database.
     */
    public static void deleteOrder(int orderId) {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(deleteStatementString);
            statement.setInt(1, orderId);
            statement.executeUpdate();
            System.out.println("Deletion complete");

            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * It returns all the orders from database.
     * @return an array list with objects of type Order or null if there are no orders in the database.
     */
    public static ArrayList<Order> showAllOrders() {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(selectStatementString);
            ResultSet resultSet = statement.executeQuery();
            ArrayList<Order> ordersArray = new ArrayList<>();
            while(resultSet.next()) {
                Order order = new Order(resultSet.getInt("id"), resultSet.getInt("clientId"), resultSet.getInt("productId"), resultSet.getInt("quantity"), resultSet.getInt("orderDetailsId"));
                ordersArray.add(order);
            }
            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(resultSet);
            return ordersArray;
        } catch (Exception e) { System.out.println(e); }
        return null;
    }

    /**
     * It searches for a client with the specified id.
     * @param clientId the id of the client which is searched for.
     * @return an array list with objects of type Client or null if there are no clients with specified name.
     */
    public static ArrayList<Order> findOrderByClientId(int clientId) {
        try {
            Connection connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(findByClientIdStatementString);
            statement.setInt(1, clientId);
            ResultSet resultSet = statement.executeQuery();
            ArrayList<Order> orderArrayList = new ArrayList<>();
            while (resultSet.next()) {
                Order order = new Order(resultSet.getInt("id"), resultSet.getInt("clientId"), resultSet.getInt("productId"), resultSet.getInt("quantity"), resultSet.getInt("orderDetailsId"));
                orderArrayList.add(order);
            }
            ConnectionFactory.close(connection);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(resultSet);
            return orderArrayList;
        } catch (Exception e) { System.out.println(e); }
        return null;
    }

}
