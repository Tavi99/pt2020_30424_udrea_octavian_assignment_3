package model;

/**
 * Contains info about a client(id, name, address).
 */
public class Client {

    private int id;
    private String name;
    private String address;

    /**
     * Constructor
     * @param id the id of the client.
     * @param name the name of the client.
     * @param address the address of the client.
     */
    public Client(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    /**
     * Gets the id of the client.
     * @return the client id.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of the client.
     * @param id new client id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the name of the client.
     * @return the client name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the client.
     * @param name new name of the client.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the address of the client.
     * @return the client's address.
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the address of the client.
     * @param address new client's address.
     */
    public void setAddress(String address) {
        this.address = address;
    }
}
