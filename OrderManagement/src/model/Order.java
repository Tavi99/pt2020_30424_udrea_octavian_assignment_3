package model;

/**
 * Contains info about an order(id, client id, product id, quantity, order details id).
 */
public class Order {

    private int id;
    private int clientId;
    private int productId;
    private int quantity;
    private int orderDetailsId;

    /**
     * Constructor.
     * @param orderId the order id
     * @param clientId the client id
     * @param productId the product id
     * @param quantity the requested quantity for a certain product
     * @param orderDetailsId the id for order details
     */
    public Order(int orderId, int clientId, int productId, int quantity, int orderDetailsId) {
        this.id = orderId;
        this.clientId = clientId;
        this.productId = productId;
        this.quantity = quantity;
        this.orderDetailsId = orderDetailsId;
    }

    /**
     * Gets the order id.
     * @return the order id.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the order id.
     * @param id new order id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the client id.
     * @return the client id.
     */
    public int getClientId() {
        return clientId;
    }

    /**
     * Sets the client id.
     * @param clientId new client id.
     */
    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    /**
     * Gets product id.
     * @return the product id.
     */
    public int getProductId() {
        return productId;
    }

    /**
     * Sets the product id.
     * @param productId new product id.
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     * Gets the quantity of the requested product.
     * @return the quantity of the requested product.
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Sets the quantity of the requested product.
     * @param quantity new quantity of the requested product.
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Gets the order details id.
     * @return the order details id.
     */
    public int getOrderDetailsId() {
        return orderDetailsId;
    }

    /**
     * Sets the order details id.
     * @param orderDetailsId new order details id.
     */
    public void setOrderDetailsId(int orderDetailsId) {
        this.orderDetailsId = orderDetailsId;
    }
}
