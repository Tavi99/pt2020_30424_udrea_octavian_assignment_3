package model;

/**
 * Contains info about an order detail(id and total price of the order).
 */
public class OrderDetail {

    private int id;
    private double totalPrice;

    /**
     * Constructor.
     * @param id the id of the order details.
     * @param totalPrice the total price of the order.
     */
    public OrderDetail(int id, double totalPrice) {
        this.id = id;
        this.totalPrice = totalPrice;
    }

    /**
     * Gets the id of the order details.
     * @return id of the order details.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of the order details.
     * @param id new id of the order details.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the total price of the order.
     * @return the total price of the order.
     */
    public double getTotalPrice() {
        return totalPrice;
    }

    /**
     * Sets the total price of the order.
     * @param totalPrice the new total price of the order.
     */
    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
}
