package model;

/**
 * Contains info about a product(id, name, quantity and price per unit).
 */
public class Product {

    private int id;
    private String name;
    private int quantity;
    private double price;

    /**
     * Constructor.
     * @param id the id of the product.
     * @param name the name of the product.
     * @param quantity the quantity of the product.
     * @param price the price of a product per unit.
     */
    public Product(int id, String name, int quantity, double price) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    /**
     * Gets the product id.
     * @return the product id.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the product id.
     * @param id new product id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets product name.
     * @return the product name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the product.
     * @param name new product name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the quantity of the product.
     * @return the quantity of the product.
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Sets the product quantity.
     * @param quantity new product quantity.
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Gets the price of the product.
     * @return the product price.
     */
    public double getPrice() {
        return price;
    }

    /**
     * Sets the price of the product.
     * @param price new product price.
     */
    public void setPrice(double price) {
        this.price = price;
    }
}
