package presentation;

import businessLogicLayer.AppManager;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * It parses a text file and executes the corresponding command for each line of the text file which was parsed.
 */
public class TextFileParser {

    /**
     * It parse a line which represents a command and executes the corresponding command.
     * @param command a string which represents the command(without details about that command).
     * @param tokens the details for the command(e.g. name and address of the client if the command is "Insert client").
     * @param reportNumber the number of the report
     */
    public static void parseCommand(String command, String[] tokens, int reportNumber) {

        switch (command) {
            case "Insert client":
                AppManager.insertClientIntoDatabase(tokens);
                break;
            case "Delete client":
                AppManager.deleteClientFromDatabase(tokens);
                break;
            case "Insert product":
                AppManager.insertProductIntoDatabase(tokens);
                break;
            case "Delete product":
                AppManager.deleteProductFromDatabase(tokens);
                break;
            case "Order":
                AppManager.createOrder(tokens);
                break;
            case "Report client":
                PdfFileGenerator.PdfReportForClients(reportNumber);
                break;
            case "Report product":
                PdfFileGenerator.PdfReportForProducts(reportNumber);
                break;
            case "Report order":
                PdfFileGenerator.PdfReportForOrders(reportNumber);
                break;
            default:
                System.out.println("There is no such a command!");
         }

    }

    /**
     * It opens and reads a command text file line by line.
     * @param path the path where the command text file is.
     */
    public static void parseCommandsTextFile(String path) {

        try {
            int reportNumber = 0;
            FileInputStream file = new FileInputStream(path);
            Scanner scanner = new Scanner(file);
            while(scanner.hasNextLine()) {

                String line = scanner.nextLine();
                String nameOfTheCommand;

                String[] tokens = line.split("[:,]+");

                for(int index = 0; index < tokens.length; index++)
                    tokens[index] = tokens[index].trim();

                nameOfTheCommand = tokens[0];
                if(nameOfTheCommand.startsWith("Report"))
                    reportNumber++;
                parseCommand(nameOfTheCommand, tokens, reportNumber);
            }
            scanner.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }
}
