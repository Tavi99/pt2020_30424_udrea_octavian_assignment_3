package presentation;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dataAccessLayer.ClientDAO;
import dataAccessLayer.OrderDAO;
import dataAccessLayer.ProductDAO;
import model.Client;
import model.Order;
import model.OrderDetail;
import model.Product;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * It generates pdf reports for displaying clients/products/order from database, for generating order bills or for
 * generating under-stock messages for a product.
 */
public class PdfFileGenerator {

    /**
     * It generates a pdf report which display all the clients from the database.
     * @param reportNumber the number of the report.
     */
    public static void PdfReportForClients(int reportNumber) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("D:\\Clients" + reportNumber + ".pdf"));
            document.open();
            Paragraph intro = new Paragraph("CLIENT TABLE");
            Paragraph space = new Paragraph("\n");
            PdfPTable table = new PdfPTable(3);
            PdfPCell c1 = new PdfPCell(new Paragraph("ID"));
            PdfPCell c2 = new PdfPCell(new Paragraph("Name"));
            PdfPCell c3 = new PdfPCell(new Paragraph("Address"));
            table.addCell(c1);
            table.addCell(c2);
            table.addCell(c3);
            ArrayList<Client> clients = ClientDAO.showAllClients();
            for(int index = 0; index < clients.size(); index++) {
                c1 = new PdfPCell(new Paragraph(Integer.toString(clients.get(index).getId())));
                c2 = new PdfPCell(new Paragraph(clients.get(index).getName()));
                c3 = new PdfPCell(new Paragraph(clients.get(index).getAddress()));
                table.addCell(c1);
                table.addCell(c2);
                table.addCell(c3);
            }
            document.add(intro);
            document.add(space);
            document.add(table);
            document.close();
        } catch (Exception e) { e.printStackTrace(); }
    }

    /**
     * It generates a pdf report which display all the products from the database.
     * @param reportNumber the number of the report.
     */
    public static void PdfReportForProducts(int reportNumber) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("D:\\Products" + reportNumber + ".pdf"));
            document.open();
            Paragraph intro = new Paragraph("PRODUCT TABLE");
            Paragraph space = new Paragraph("\n");
            PdfPTable table = new PdfPTable(4);
            PdfPCell c1 = new PdfPCell(new Paragraph("ID"));
            PdfPCell c2 = new PdfPCell(new Paragraph("Name"));
            PdfPCell c3 = new PdfPCell(new Paragraph("Quantity"));
            PdfPCell c4 = new PdfPCell(new Paragraph("Price"));
            table.addCell(c1);
            table.addCell(c2);
            table.addCell(c3);
            table.addCell(c4);
            ArrayList<Product> products = ProductDAO.showAllProducts();
            for(int index = 0; index < products.size(); index++) {
                c1 = new PdfPCell(new Paragraph(Integer.toString(products.get(index).getId())));
                c2 = new PdfPCell(new Paragraph(products.get(index).getName()));
                c3 = new PdfPCell(new Paragraph(Integer.toString(products.get(index).getQuantity())));
                c4 = new PdfPCell(new Paragraph(Double.toString(products.get(index).getPrice())));
                table.addCell(c1);
                table.addCell(c2);
                table.addCell(c3);
                table.addCell(c4);
            }
            document.add(intro);
            document.add(space);
            document.add(table);
            document.close();
        } catch (Exception e) { e.printStackTrace(); }
    }

    /**
     * It generates a pdf report which display all the orders from the database.
     * @param reportNumber the number of the report.
     */
    public static void PdfReportForOrders(int reportNumber) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("D:\\Orders" + reportNumber + ".pdf"));
            document.open();
            Paragraph intro = new Paragraph("ORDER TABLE");
            Paragraph space = new Paragraph("\n");
            PdfPTable table = new PdfPTable(5);
            PdfPCell c1 = new PdfPCell(new Paragraph("ID"));
            PdfPCell c2 = new PdfPCell(new Paragraph("ClientID"));
            PdfPCell c3 = new PdfPCell(new Paragraph("ProductID"));
            PdfPCell c4 = new PdfPCell(new Paragraph("Quantity"));
            PdfPCell c5 = new PdfPCell(new Paragraph("OrderDetailsID"));
            table.addCell(c1);
            table.addCell(c2);
            table.addCell(c3);
            table.addCell(c4);
            table.addCell(c5);
            ArrayList<Order> orders = OrderDAO.showAllOrders();
            for(int index = 0; index < orders.size(); index++) {
                c1 = new PdfPCell(new Paragraph(Integer.toString(orders.get(index).getId())));
                c2 = new PdfPCell(new Paragraph(Integer.toString(orders.get(index).getClientId())));
                c3 = new PdfPCell(new Paragraph(Integer.toString(orders.get(index).getProductId())));
                c4 = new PdfPCell(new Paragraph(Integer.toString(orders.get(index).getQuantity())));
                c5 = new PdfPCell(new Paragraph(Integer.toString(orders.get(index).getId())));
                table.addCell(c1);
                table.addCell(c2);
                table.addCell(c3);
                table.addCell(c4);
                table.addCell(c5);
            }
            document.add(intro);
            document.add(space);
            document.add(table);
            document.close();
        } catch (Exception e) { e.printStackTrace(); }
    }

    /**
     * It generates a pdf report which display an under-stock message for a specified product.
     * @param product the product for which the under-stock message will be shown.
     */
    public static void PdfReportUnderStockMessage(Product product) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("D:\\UnderStock" + product.getQuantity() + ".pdf"));
            document.open();
            Paragraph message = new Paragraph("Under-stock: product = " + product.getName() + "|Available quantity = " + product.getQuantity());

            document.add(message);
            document.close();

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * It generates a pdf report with order details(like: client name, order id, product name, requested quantity of the product, total price).
     * @param order the order which was made.
     * @param product the product which is in the order.
     * @param client the client who made the order.
     * @param orderDetail the details of the order.
     * @param requestedQuantity the requested quantity for the product.
     */
    public static void PdfBill(Order order, Product product, Client client, OrderDetail orderDetail, int requestedQuantity) {
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("D:\\Bill for order " + orderDetail.getId() + ".pdf"));
            document.open();
            Paragraph intro = new Paragraph("Bill for order " + orderDetail.getId());
            PdfPTable table = new PdfPTable(5);
            PdfPCell c1 = new PdfPCell(new Paragraph("Client Name"));
            PdfPCell c2 = new PdfPCell(new Paragraph("Order ID"));
            PdfPCell c3 = new PdfPCell(new Paragraph("Product"));
            PdfPCell c4 = new PdfPCell(new Paragraph("Quantity"));
            PdfPCell c5 = new PdfPCell(new Paragraph("Total Price"));
            table.addCell(c1);
            table.addCell(c2);
            table.addCell(c3);
            table.addCell(c4);
            table.addCell(c5);
            c1 = new PdfPCell(new Paragraph(client.getName()));
            c2 = new PdfPCell(new Paragraph(Integer.toString(order.getId())));
            c3 = new PdfPCell(new Paragraph(product.getName()));
            c4 = new PdfPCell(new Paragraph(Integer.toString(requestedQuantity)));
            c5 = new PdfPCell(new Paragraph(Double.toString(orderDetail.getTotalPrice())));
            table.addCell(c1);
            table.addCell(c2);
            table.addCell(c3);
            table.addCell(c4);
            table.addCell(c5);
            document.add(intro);
            document.add(table);
            document.close();
        } catch (Exception e) { e.printStackTrace(); }
    }
}
